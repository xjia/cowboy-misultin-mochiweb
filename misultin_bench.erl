-module(misultin_bench).
-export([start/1, stop/0, handle_http/1]).

start(Port) ->
  misultin:start_link([{port, Port}, {loop, fun(Req) -> handle_http(Req) end}]).

stop() ->
  misultin:stop().

handle_http(Req) ->
  % get value parameter
  Args = Req:parse_qs(),
  Value = misultin_utility:get_key_value("value", Args),
  case Value of
    undefined ->
      Req:ok([{"Content-Type", "text/xml"}], ["<http_test><error>no value specified</error></http_test>"]);
    _ ->
      Req:ok([{"Content-Type", "text/xml"}], ["<http_test><value>", Value, "</value></http_test>"])
  end.

