-module(cowboy_bench_handler).
-export([init/3, handle/2, terminate/2]).

init(_Transport, Req, []) ->
  {ok, Req, undefined}.

handle(Req, State) ->
  {ok, Req2} = case cowboy_http_req:qs_val(<<"value">>, Req) of
    {undefined, _} ->
      cowboy_http_req:reply(200, [{<<"Content-Type">>, <<"text/xml">>}], <<"<http_test><error>no value specified</error></http_test>">>, Req);
    {Value, _} ->
      cowboy_http_req:reply(200, [{<<"Content-Type">>, <<"text/xml">>}], ["<http_test><value>", Value, "</value></http_test>"], Req)
  end,
  {ok, Req2, State}.

terminate(_Req, _State) ->
  ok.

