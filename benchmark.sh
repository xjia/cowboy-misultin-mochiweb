#!/bin/bash
#
# cowboy:9090
# mochiweb:9091
# misultin:9092
# nodejs:9093
# tornado:9094
#
ts=`date +%Y-%m-%d-%H-%M-%S` # generate a timestamp each time we run the test
echo "Test on $ts"
rm -f $ts.909?.log
for port in `seq 9090 9094`; do
  echo "Port $port"
  for rate in `seq 100 25 1200`; do
    echo "Rate $rate"
    free -m >> $ts.$port.log
    vmstat >> $ts.$port.log
    sleep 5 # let the system have a rest
    httperf --hog --timeout=5 --client=0/1 --server=localhost --port=$port --uri=/?value=benchmarks --rate=$rate --send-buffer=4096 --recv-buffer=16384 --num-conns=5000 --num-calls=10 >> $ts.$port.log
  done
done

