-module(cowboy_bench).
-export([start/1, stop/0]).

start(Port) ->
  application:start(ranch),
  application:start(cowboy),
  Dispatch = [
    {'_', [{'_', cowboy_bench_handler, []}]}
  ],
  cowboy:start_listener(http, 100,
    cowboy_tcp_transport, [{port, Port}],
    cowboy_http_protocol, [{dispatch, Dispatch}]
  ).

stop() ->
  application:stop(cowboy),
  application:stop(ranch).

