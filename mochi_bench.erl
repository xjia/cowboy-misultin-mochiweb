-module(mochi_bench).
-export([start/1, stop/0, handle_http/1]).

start(Port) ->
  mochiweb_http:start([{port, Port}, {loop, fun(Req) -> handle_http(Req) end}]).

stop() ->
  mochiweb_http:stop().

handle_http(Req) ->
  % get value parameter
  Args = Req:parse_qs(),
  Value = misultin_utility:get_key_value("value", Args),
  case Value of
    undefined ->
      Req:respond({200, [{"Content-Type", "text/xml"}], ["<http_test><error>no value specified</error></http_test>"]});
    _ ->
      Req:respond({200, [{"Content-Type", "text/xml"}], ["<http_test><value>", Value, "</value></http_test>"]})
  end.

